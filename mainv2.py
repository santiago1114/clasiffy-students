from datetime import date
import pandas as pd
import datetime
import mechanicalsoup
import re

def get_students_data(calificacionesUrl,
                      ultvezUrl,
                      asistenciasUrl):

    browser.open(calificacionesUrl)

    html = browser.get_current_page() #soupObject

    studentsRaw = html.find_all("a", {"class": "username"})
    studentsIdsRaw = html.find_all("td", {"class": "userfield useridnumber cell c2"})
    emailRaw = html.find_all("td", {"class": "userfield useremail cell c3"})
    reto1Raw = html.find_all("td", class_=re.compile(r"^grade .+ grade_type_value cell c4$"))
    reto2Raw = html.find_all("td", class_=re.compile(r"^grade .+ grade_type_value cell c5$"))
    reto3Raw = html.find_all("td", class_=re.compile(r"^grade .+ grade_type_value cell c6$"))
    reto4Raw = html.find_all("td", class_=re.compile(r"^grade .+ grade_type_value cell c7$"))
    reto5Raw = html.find_all("td", class_=re.compile(r"^grade .+ grade_type_value cell c8$"))
    totalRaw = html.find_all("td", class_=re.compile(r"^grade .+ course grade_type_scale cell c.. lastcol$"))

    # Extrae datos de la tabla de calificaciones
    students = [" ".join(student.get_text().strip().upper().split()) for student in studentsRaw]
    studentsIds = [id.get_text() for id in studentsIdsRaw]
    email = [e.get_text() for e in emailRaw]
    reto1 = [nota.get_text() for nota in reto1Raw]
    reto2 = [nota.get_text() for nota in reto2Raw]
    reto3 = [nota.get_text() for nota in reto3Raw]
    reto4 = [nota.get_text() for nota in reto4Raw]
    reto5 = [nota.get_text() for nota in reto5Raw]
    total = [nota.get_text() for nota in totalRaw]

    retosPresentados = [0] * len(reto1)

    for i in range(len(reto1)):
        if reto1[i] != "-":
            retosPresentados[i] += 1
        if reto2[i] != "-":
            retosPresentados[i] += 1
        if reto3[i] != "-":
            retosPresentados[i] += 1
        if reto4[i] != "-":
            retosPresentados[i] += 1
        if reto5[i] != "-":
            retosPresentados[i] += 1


    # Indica la fecha de consulta de datos
    today = datetime.datetime.now()
    today = today.strftime("%d/%m/%Y, %H:%M")
    today = [today for i in range(len(students))]


    browser.open(ultvezUrl)
    html = browser.get_current_page()

    # Extrae los datos de la ultima vez de acceso al aula
    ultVezIdsRaw = html.find_all("td", {"class": "cell c2"})
    rolRaw = html.find_all("td", {"class": "cell c4"})
    ultVezRaw = html.find_all("td", {"class": "cell c6"})
    rol = [rolStr.get_text() for rolStr in rolRaw if rolStr.get_text() != '']
    indices = [i for i, x in enumerate(rol) if x == "Estudiante"]

    ultVezIds = [ultVezIdsRaw[i].get_text() for i in indices]
    findIds = [ultVezIds.index(studentsIds[i]) for i in range(len(studentsIds))]
    ultVez = [ultVezRaw[i].get_text() for i in indices]
    ultVez = [ultVez[i] for i in findIds]


    browser.open(asistenciasUrl)
    html = browser.get_current_page()

    # Extrae los datos de la tabla de asistencias
    idsRaw = html.find_all("td", {"class": "left nowrap cell c1"})
    asisRaw = html.find_all("td", {"class": "center narrow nowrap contrast cell c11"})
    asis = [a.get_text() for a in asisRaw]
    ids = [id.get_text() for id in idsRaw]
    findIds = [ids.index(studentsIds[i]) for i in range(len(studentsIds))]
    asis = [asis[i] for i in findIds]


    data = {'Fecha consulta': today,
            'Estudiante': students,
            'Correo': email,
            'Ultimo Acceso': ultVez,
            'Asistencias': asis,
            'Reto 1': reto1,
            'Reto 2': reto2,
            'Reto 3': reto3,
            'Reto 4': reto4,
            'Reto 5': reto5,
            'Retos presentados': retosPresentados,
            'Nota final': total}


    df = pd.DataFrame(data)
    return df


# Inicia sesion
mainUrl = "https://lms.uis.edu.co/mintic/login/index.php"
browser = mechanicalsoup.StatefulBrowser()
browser.open(mainUrl)
browser.get_url()

browser.select_form('form[action="https://lms.uis.edu.co/mintic/login/index.php"]')

browser["username"] = "" # Colocar el usuario de Moodle
browser["password"] = "" # Colocar la contraseña del usuario en Moodle

browser.submit_selected()


dataFrames = []
# Cambiar los ids segun cursos a consultar
idAsistenciasParam = [33895, 33655, 33295, 33535, 33415, 33175, 32815, 33055, 35935]
idGnralParam = list(range(289,297)) + [278]
ids = [ (idGnralParam[i],idAsistenciasParam[i]) for i in range(len(idGnralParam))]
for i, i2 in ids:
    print(f"Extrayendo grupo con id: {i}")
    dataFrames.append(get_students_data(
        "https://lms.uis.edu.co/mintic/grade/report/grader/index.php?id={}&sortitemid=lastname".format(i), 
        "https://lms.uis.edu.co/mintic/user/index.php?id={}&perpage=5000&tsort=lastname".format(i), 
        "https://lms.uis.edu.co/mintic/mod/attendance/report.php?showsessiondetails=0&id={}&sort=1&page=all&view=7".format(i2)
        ))

writer = pd.ExcelWriter('Reporte_rendimiento_tripulantes_J61_a_J70.xlsx', engine='xlsxwriter')
sheetNames = list(range(61,69)) + [70];
excelDataVar = [ (dataFrames[i],"J"+str(sheetNames[i])) for i in range(len(sheetNames))]
for df, sn in excelDataVar:
    df.to_excel(writer, sheet_name=sn, index=False)

browser.close() 
writer.save()
